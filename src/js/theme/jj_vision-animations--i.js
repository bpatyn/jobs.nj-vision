(function($) {

	var tl = new TimelineMax({paused: true}),
			start = $('#start');

	tl.to(start, 0.15, {morphSVG:"#step1"})
	  .to(start, 0.2, {morphSVG:"#step2"})
	  .to(start, 0.15, {morphSVG:"#step3"})
	  .to(start, 0.15, {morphSVG:"#step4"})
	  .to(start, 0.15, {morphSVG:"#step5"})
	  .to(start, 0.15, {morphSVG:"#step6"})
	  .to(start, 0.15, {morphSVG:"#step7"})
	  .to(start, 0.15, {morphSVG:"#end"});

	$('#animate-now').click(function(event) {
	  tl.play();
  });

  $('#animate-now').click(function(event) {
  	if (!$(this).hasClass('end')) {
		  tl.play();
		  $(this).addClass('end');
  	} else {
  		tl.reverse();
  		$(this).removeClass('end');
  	}
  });


})(jQuery);