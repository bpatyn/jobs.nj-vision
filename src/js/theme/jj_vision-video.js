(function($) {


      var   curtain         = $('.video-section__curtain'),
            theaterMode     = false,
            close           = $('.video-section__close'),
            openCurtainTl   = new TimelineMax({ paused: true, onReverseComplete:removeActive });

      openCurtainTl.timeScale(1.5)
            .set(curtain, { height: "0", skewY: "0", marginBottom: "0" })
            .to(curtain, 0.15, { scaleX:1.1, height: "20%", skewY: "5deg", marginBottom: "-150px" })
            .to(curtain, 0.15, { scaleY:1.2, height: "60%", skewY: "-10deg" }, "+=0.1")
            .to(curtain, 0.15, { height: "85%", skewY: "4deg", marginBottom: "-30px" }, "+=0.15")
            .to(curtain, 0.07, { height: "100%", skewY: "0", marginBottom: "0" }, "+=0.15")
            .call(showVideo);

      /* Click actions on play button */
      $('.video-section__play').click(function(event) {
        console.log('you clicked play');
        openCurtainTl.play();
        curtain.addClass('is-active');
        theaterMode = true;
      });

      /* Keyboard interactions */
      $(document).keyup(function(e) {
        if ((e.keyCode == 27) && theaterMode == true) { // escape key maps to keycode `27`
          console.log('you are in theater mode');
          openCurtainTl.reverse();
        }
      });

      close.click(function() {
        close.removeClass('is-visible');
        openCurtainTl.reverse();
      });

      function removeActive() {
        curtain.removeClass('is-active');
      }

      function showVideo() {
        close.addClass('is-visible');
      }

})(jQuery);