(function($) {

	var tl = new TimelineMax({paused: true}),
			start = $('#start');

	tl.to(start, 0.15, {morphSVG:"#step1"})
	  .to(start, 0.2, {morphSVG:"#step2"})
	  .to(start, 0.3, {morphSVG:"#step3"})
	  .to(start, 0.25, {morphSVG:"#step4"})
	  .to(start, 0.15, {morphSVG:"#step5"})
	  .to(start, 0.2, {morphSVG:"#end", ease: Power2.easeIn});

	$('#animate-now').click(function(event) {
	  tl.play();
  });

  $('#animate-now').click(function(event) {
  	if (!$(this).hasClass('end')) {
		  tl.play();
		  $(this).addClass('end');
  	} else {
  		tl.reverse();
  		$(this).removeClass('end');
  	}
  });


})(jQuery);