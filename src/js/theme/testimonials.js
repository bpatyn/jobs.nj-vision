  (function($) {
    "use strict";

    var testimonialTeaser = $('.testimonial_teaser'),
        testimonialMore   = $('#testimonial-more'),
        testimonialQa     = $('.testimonial__q-a'),
        testimonialFade   = $('.testimonial__fadeout'),
        activeTestimonial = $('.testimonial_teaser.is-active');

    // Drupal.behaviors.slickSlide = {
    //   attach: function() {

    /* Slick slider settings */
    $(".slickslider").slick({
      dots: false,
      infinite: false,
      speed: 300,
      arrows: true,
      autoplay: false, // set to true for production site
      slidesToShow: 3,
      slidesToScroll: 3,
      swipeToSlide: true,
      variableWidth: true,
      draggable: true,
      responsive: [{
          breakpoint: 1024,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 300,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    });
    //   }
    // };
    

    /* Add is-active class to testimonial teaser when clicked and remove from previous */
    testimonialTeaser.click(function(event) {
      if (!$(this).hasClass('is-active')) {
        $('.testimonial_teaser.is-active').removeClass('is-active');
        $(this).addClass('is-active');
      }
    });

    // /* Add is-active class to Q&A on button click */
    // testimonialMore.click(function(event) {
    //   if (!testimonialQa.hasClass('is-active')) {
    //     testimonialQa.addClass('is-active');
    //   }
    // });

    // For each story, determine its natural height and store it as data.
    // This is encapsulated into a self-executing function to isolate the 
    // variables from other things in my script.
    (function(){

      // First I grab the collapsed height that was set in the css for later use
      var collapsedHeight = testimonialQa.css('maxHeight');

      // Now for each story, grab the scrollHeight property and store it as data 'natural'        
      testimonialQa.each(function(){
        var $this = $(this);
        $this.data('natural', $this[0].scrollHeight);
      });

      // Now, set-up the handler for the toggle buttons
      testimonialMore.bind('click', function(){
        var $qa = testimonialQa,
            duration = 250; // animation duration

        // I use a class 'expanded' as a flag to know what state it is in,
        // and to make style changes, as required.  
        if ($qa.hasClass('expanded')) {
          // If it is already expanded, then collapse it using the css figure as
          // collected above and remove the expanded class
          $qa.animate({'maxHeight': collapsedHeight}, duration, function() {
            $qa.removeClass('expanded');
            testimonialMore.html('Continue Reading');
          });
          testimonialFade.show();
        }
        else {
          // If it is not expanded now, then animate the max-height to the natural
          // height as stored in data, then add the 'expanded' class
          $qa.animate({'maxHeight': $qa.data('natural')}, duration, function() {
            $qa.addClass('expanded');
            testimonialMore.html('Hide story');
          });
          testimonialFade.hide();
        }
      });

    })(); // end anonymous, self-executing function


  })(jQuery);