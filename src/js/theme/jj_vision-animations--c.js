(function($) {

	var tl = new TimelineMax({paused: true}),
			start = $('#start');

	tl.to(start, 0.3, {morphSVG:"#step1"})
	  .to(start, 0, {morphSVG:"#step2"})
	  .to(start, 0.5, {morphSVG:"#end", ease: Power2.easeOut});

  $('#animate-now').click(function(event) {
  	if (!$(this).hasClass('end')) {
		  tl.play();
		  $(this).addClass('end');
  	} else {
  		tl.reverse();
  		$(this).removeClass('end');
  	}
  });


})(jQuery);