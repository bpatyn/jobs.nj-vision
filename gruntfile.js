module.exports = function(grunt) {

  //Load modules
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');

  //tasks config
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      files: ['src/js/**/*', 'src/css/**/*'],
      tasks: ['sass', 'uglify'],
      options: {}
    },

    sass: {
      options: {
        sourceMap: true,
        outputStyle: 'compressed'
      },
      static_mappings: {
        files: {
          'assets/css/theme/jj_vision_theme.css': 'src/css/theme/jj_vision_theme.scss',
        }
      },
      dependencies: {
        files: [
          {
            expand: true,
            cwd: 'src/css/dependencies/',
            src: ['**/*.scss'],
            dest: 'assets/css/dependencies/',
            flatten: true,
            ext: '.css'
          }
        ]
      },
      plugins: {
        files: [
          {
            expand: true,
            cwd: 'src/css/plugins/',
            src: ['**/*.scss'],
            dest: 'assets/css/plugins/',
            flatten: true,
            ext: '.css'
          }
        ]
      },
      pages: {
        files: [
          {
            expand: true,
            cwd: 'src/css/pages/',
            src: ['**/*.scss'],
            dest: 'assets/css/pages/',
            flatten: true,
            ext: '.css'
          }
        ]
      }
    },

    uglify: {
      options: {
        compress: false,
        beautify: false,
        sourceMap: true
      },
      static_mappings: {
        files: {
          'assets/js/theme/jj_vision-video.js': 'src/js/theme/jj_vision-video.js',
          'assets/js/theme/testimonials.js': 'src/js/theme/testimonials.js',
          'assets/js/theme/jj_vision-animations--a.js': 'src/js/theme/jj_vision-animations--a.js',
          'assets/js/theme/jj_vision-animations--p.js': 'src/js/theme/jj_vision-animations--p.js',
          'assets/js/theme/jj_vision-animations--i.js': 'src/js/theme/jj_vision-animations--i.js',
          'assets/js/theme/jj_vision-animations--c.js': 'src/js/theme/jj_vision-animations--c.js',
          'assets/js/theme/jj_vision-animations--e.js': 'src/js/theme/jj_vision-animations--e.js',
          'assets/js/theme/jj_vision-animations--n.js': 'src/js/theme/jj_vision-animations--n.js',
        }
      },
      dependencies: {
        files: [
          {
            expand: true,
            cwd: 'src/js/dependencies/',
            src: ['**/*.js'],
            dest: 'assets/js/dependencies/',
            flatten: true,
            ext: '.js'
          }
        ]
      },
      plugins: {
        files: [
          {
            expand: true,
            cwd: 'src/js/plugins/',
            src: ['**/*.js'],
            dest: 'assets/js/plugins/',
            flatten: true,
            ext: '.js'
          }
        ]
      },
    }
  });

  //Task(s)/Commands
  grunt.registerTask('default', ['sass']);
};